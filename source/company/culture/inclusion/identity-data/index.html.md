---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2019-05-31

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Based in the US                           | 389   | 60.97%      |
| Based in the UK                           | 31    | 4.86%       |
| Based in the Netherlands                  | 23    | 3.61%       |
| Based in Other Countries                  | 195   | 30.56%      |
| Total Team Members                        | 638   | 100%        |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men                                       | 468   | 73.35%      |
| Women                                     | 170   | 26.65%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 638   | 100%        |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Leadership                         | 36    | 75.00%      |
| Women in Leadership                       | 12    | 25.00%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 48    | 100%        |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Men in Development                        | 248   | 82.94%      |
| Women in Development                      | 51    | 17.06%      |
| Other Gender Identities                   | 0     | 0%          |
| Total Team Members                        | 299   | 100%        |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 26    | 6.68%       |
| Black or African American                 | 9     | 2.31%       |
| Hispanic or Latino                        | 26    | 6.68%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.26%       |
| Two or More Races                         | 15    | 3.86%       |
| White                                     | 226   | 58.10%      |
| Unreported                                | 86    | 22.11%      |
| Total Team Members                        | 389   | 100%        |

| Race/Ethnicity in Development (US Only)   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 11    | 8.80%       |
| Black or African American                 | 2     | 1.60%       |
| Hispanic or Latino                        | 8     | 6.40%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 7     | 5.60%       |
| White                                     | 73    | 58.40%      |
| Unreported                                | 24    | 19.20%      |
| Total Team Members                        | 125   | 100%        |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 5     | 12.82%      |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 0     | 0.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 2.56%       |
| Two or More Races                         | 1     | 2.56%       |
| White                                     | 22    | 56.41%      |
| Unreported                                | 10    | 25.64%      |
| Total Team Members                        | 39    | 100%        |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 23    | 9.24%       |
| Black or African American                 | 7     | 2.81%       |
| Hispanic or Latino                        | 10    | 4.02%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 5     | 2.01%       |
| White                                     | 126   | 50.60%      |
| Unreported                                | 78    | 31.33%      |
| Total Team Members                        | 249   | 100%        |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 16    | 9.20%       |
| Black or African American                 | 4     | 2.30%       |
| Hispanic or Latino                        | 10    | 5.75%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 4     | 2.30%       |
| White                                     | 89    | 51.15%      |
| Unreported                                | 51    | 29.31%      |
| Total Team Members                        | 174   | 100%        |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Asian                                     | 0     | 0.00%       |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 1     | 11.11%      |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 0     | 0.00%       |
| White                                     | 3     | 33.33%      |
| Unreported                                | 5     | 55.56%      |
| Total Team Members                        | 9     | 100%        |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| 18-24                                     | 18    | 2.82%       |
| 25-29                                     | 128   | 20.06%      |
| 30-34                                     | 179   | 28.06%      |
| 35-39                                     | 123   | 19.28%      |
| 40-49                                     | 130   | 20.38%      |
| 50-59                                     | 53    | 8.31%       |
| 60+                                       | 7     | 1.10%       |
| Unreported                                | 0     | 0.00%       |
| Total Team Members                        | 638   | 100%        |
