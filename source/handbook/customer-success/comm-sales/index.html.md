---
layout: markdown_page
title: "Commercial Sales - Customer Success"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as worldwide sales for the mid-market and small/medium business segments. [Sales segmentation](https://about.gitlab.com/handbook/business-ops/#segmentation) is defined by the total employee count of the global account. The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Technical Account Managers (TAM).

## On this page
{:.no_toc}

- TOC
{:toc}

## Role & Responsibilities

### Solutions Architects

Solutions Architects are aligned to the Commercial Sales Account Executives by a pooled model. Requests for an SA will be pulled from the triage board by an SA based on multiple factors including availability, applicable subject matter expertise, and workload.  

* [Solutions Architect role description](/job-families/sales/solutions-architect/)
* [Solutions Architect overview](/handbook/customer-success/solutions-architects/)
* [When and how to engage a Commercial Solutions Architect](/handbook/customer-success/solutions-architects/#commercial-engagement-model)
* [How incoming SA requests are triaged](/handbook/customer-success/solutions-architects/#triage-of-issues)

### Technical Account Managers

Technical Account Managers that support Commercial Sales are aligned by region, Americas East, Americas West, EMEA and APAC. Not all accounts will have a dedicated TAM. Account qualification is required.

* [Technical Account Manager role description](/job-families/sales/technical-account-manager/)
* [Technical Account Manager overview](/handbook/customer-success/tam/)
* [When and how a TAM is engaged](/handbook/customer-success/tam/engagement/)

## Seamless Customer Journey

A seamless customer journey requires a continuous flow of relevant information between the roles at GitLab with customer outcomes in focus. Below are some examples of the transfer of information between roles that may be required.

### TAM to TAM (existing accounts)

* Ensure all accounts have a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Update TAM name on account team in Salesforce
* Share any Outreach sequences or templates currently in use
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew) 

### Account Executive to TAM (existing accounts without a TAM)

* SA or TAM creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Add TAM name to account team in Salesforce
* Identify any relevant Outreach sequences or templates
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew) 

### SA to TAM (new accounts)

* SA creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Ensure any account notes held outside the project are linked to Salesforce and shared with the TAM
* SA to clearly outline to TAM how far the customer is in their adoption of GitLab
* TAM begins Outreach sequence for new customers
* Add TAM name to account team in Salesforce, ensure SA name is already present
* Ensure any urgent action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew) 

### SA to SA (new accounts)

* Update SA name on account team in Salesforce
* Ensure any account notes are linked to Salesforce and shared with the new SA
* Introduce new SA live on a client call
* If a [POC](/handbook/sales/POC/) is pending or active, update the POC record in Salesforce as required
* Ensure any current action items are identified via an issue on the [Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966) 

## Customer Engagement

### Communication with Customers

For commercial accounts, we do not currently offer our customers Slack channels unless they meet specific criteria and it would be highly beneficial to both the customer and GitLab teams. Prior to creating the channel and provisioning access, the TAM for the account must approve its creation and use. To qualify, the customer should be at minimum:
- Premium/Silver or above
- 200k+ IACV

[Collaborative projects](https://about.gitlab.com/handbook/customer-success/tam/engagement/#managing-the-customer-engagement) should always be suggested first. Exceptions may apply case by case (reference customer, early adopter, strategic partnership, etc). If the customer is evaluating GitLab or doing a POC for an upgrade, SAs or TAMs can request an ephemeral Slack channel, but the channel should close after 90 days if it does meet sufficient criteria as shown above. Slack is simply too intensive to scale for MM and SMB.

Meanwhile, it is not always scalable for every customer to have a collaboration project either, and the CS team will determine the need for a project on a per-customer basis.
