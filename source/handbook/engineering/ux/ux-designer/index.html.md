---
layout: markdown_page
title: "Product Designer Workflow"
---

### On this page

{:.no_toc}

- TOC
{:toc}

## Product Designer

Like other departments at GitLab, we follow the [GitLab Workflow](/handbook/communication/#everything-starts-with-an-issue) and the [Engineering Workflow](/handbook/engineering/workflow/). Reviewing those resources is a good starting point for context on the Product Designer workflow below.

### Product Designer onboarding

If you are just starting out here at GitLab, welcome! Make sure to review all the pages here in the UX section of the handbook, they will help you get oriented. There is also a specific page dedicated to [Product Designer onboarding](/handbook/engineering/ux/uxdesigner-onboarding).

### Working on issues

#### Scheduling of issues in a milestone
All issues in a milestone labeled [Deliverable](https://gitlab.com/groups/gitlab-org/issues?state=opened&label_name%5B%5D=Deliverable) that need UX will be assigned to a Product Designer by the kickoff. The Product Designer&#40;s&#41; for a given area should coordinate with the PM and their backup during scheduling for any work that is critical. Product Designers assign themselves to issues in a milestone and should aim to schedule 80% of their capacity to work on responsibilities as outlined in the [role descriptions](https://about.gitlab.com/job-families/engineering/product-designer/), blocking off the remaining 20% for other proactive work. This can be anything from exploring GitLab as a product to exploring competitive products and how GitLab compares to them to conducting their own research and interacting with users and other team members.

Issues labeled [Stretch](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Stretch) may or may not be assigned to a Product Designer by the kickoff.

#### Priority for UX issues
UX works on issues in the following order:
* Has the current release milestone and is labeled Deliverable
* [UX OKRs](https://about.gitlab.com/company/okrs/) 
* Has the current release milestone and is labeled Stretch
* Has the Pajamas label
* Has the next release milestone
* [Has the **Accepting merge requests label**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=Accepting+merge+requests&label_name[]=UX)
* [Has milestone **Next 2-3 months**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Next+2-3+months&label_name[]=UX)
* [Has milestone **Next 3-6 months**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Next+3-6+months&label_name[]=UX)
* [Has milestone **Backlog**](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&milestone_title=Backlog&label_name[]=UX)
* [Popular or with high community involvement (number of comments or upvotes)](https://gitlab.com/groups/gitlab-org/issues?label_name[]=UX&sort=upvotes_desc&state=opened)
* [Everything else](https://gitlab.com/groups/gitlab-org/-/issues?state=opened&label_name[]=UX)

#### Work
Define the opportunity:
* [Product Managers (PMs)](https://about.gitlab.com/job-families/product/product-manager/) and Product Designers may seem different, but they’re actually natural product partners. Both do their best work when collaborating to understand user needs and synthesizing solutions that maximize customer results.
* A PM’s primary role is to interact with customers to understand what they need most and why, and a Product Designer’s primary role is to interact with customers to determine how to best meet those needs. 
* Over communicate with your PM(s). The collaboration and context they provide are essential to delivering UX strategies and solutions that meet or exceed customer expectations.

Before you design:
* Work with your PM to understand the who, what, and why. For example, "As a (who), I want (what), so I can (why/value)." You'll need this context to design valuable solutions for users. If you’re being asked to jump to a conclusion and implement a non-evidence based how, then ask the PM to refocus on the who, what, and why, so everyone can work together to find the best how. 
* Work with your PM to define MVP or MVC success criteria. Be sure to prioritize success criteria into MVP/MVC “must-haves” and non-MVP/MVC “should-haves” and “could-haves.” Note that this success criteria is subject to change based on new learning. The iterative design process and customer feedback, for example, often result in information that refines success criteria, which is a good thing.
* Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). If the users are unknown, be sure to learn who they are and add this information as soon as possible. 
* Investigate whether there is [existing UX Research](https://about.gitlab.com/handbook/engineering/ux/ux-research#ux-research-archive) or data that could help inform your decisions and measure results. If there isn't, consider looping in UX Researchers to conduct (or assist you in conducting) research for the problem.
* Consider conducting competitive analysis to inform your work. Look for terminology, functionality, and UX conventions that can help the team refine success criteria. Only stray from industry conventions with strategic intent, such as capitalizing on disruptive value innovation opportunities. We want users to migrate from other tools to ours, and they’re more likely to be successful and satisfied when our products use conventions that are familiar based on other tools they've used. 

Ideate and iterate:
* As you ideate and iterate, keep your designs as lo-fi as you can, and only increase fidelity as design confidence grows and implementation requires. This keeps the cost of change low, allowing you iterate and deliver more. 
Ask for PM feedback throughout the design process. They can help you sharpen the solution and counter your personal biases. Visualization articulation—through story maps, workflow diagrams and/or prototypes—also helps everyone refine their understanding of the problem and solution criteria. 
* As time permits, get feedback from other designers to help you improve your work. At minimum, they can give you objective feedback and new ideas that lead to better solutions. They may also provide context you didn’t know you were missing, such as GitLab-specific or industry-standard design conventions. Shoot for at least two feedback sessions per milestone for every important deliverable.
* Loop in engineering peers early and often. Their insight into technical costs and feasibility is essential to determining viable designs and MVPs/MVCs. Also, encourage design feedback, especially if you’re solving for a development workflow they’re familiar with. 

Refine MVP/MVC:
* UX issues have a tendency to expand in scope. Work with your PM and developers to revisit MVP/MVC and which aspects of the design are “must haves” versus those that can be pushed until later. Once you agree on MVP/MVC, document non-MVP/MVC concepts, research, and UX should- and could-haves into new issues. Mark the new issues as related to the original issue. If you’re ever unsure how to split apart large issues, work with your UX Manager.
* Developers should be able to ship working software within one release. If a feature is too large to ship within one release, work with your PM and Engineering team to determine the best way to split the feature into smaller segments.
* Encourage developers to scope down features into multiple merge requests for an easier, more efficient review process. When breaking down features into multiple merge requests, consider how the UX of the application will be affected. If merging only a portion of the total changes will negatively impact the overall experience, consider using a feature branch or feature flag. Utilizing these options will ensure that the full UX scope is shipped together.
* When breaking solutions into smaller parts, make sure to share the end design goal, so that the entire team has context. Giving everyone the full picture helps developers write code aimed at achieving that goal in the future.
* Keep the issue description updated with the agreed-on scope, even if doesn’t impact your work. This is everyone’s responsibility. The issue description must be the Single Source Of Truth (SSOT), not the discussion or individual comments.

Propose an MVP/MVC solution:
* Propose one solution, not multiple alternative solutions for others to pick, as this undermines your position as a UX expert and leads to [design by committee](https://en.wikipedia.org/wiki/Design_by_committee). If you have a good reason to propose multiple alternative solutions, make sure to explain why.
* Remember that proposal prototypes should default to lo-fi and only increase fidelity as needed. To avoid waste, ask yourself, “What’s the simplest prototype I can produce to communicate the solution, get the feedback I need, and ensure this is implemented well?”
* Proposals don't have to have images. Sometimes words can paint a surprisingly good picture of how an experience should feel, look, and behave.
* When sharing asynchronously, make sure your audience has the context necessary to understand your proposal and how they can help. Is it clear who will use the solution and what it will enable them to accomplish?  Do you need feedback or assistance from stakeholders? If so, on what specifically?  Or, are you looking for approval? To make reviewing easier, have you highlighted things that changed since the last review?
* @mention your UX Manager on the issue for awareness and feedback.
* Frame design discussions around the customer and the problem being solved, not the UI or functionality itself. When presenting, start with the current state and how it fails to meet user needs, and then walk through the proposed solution from the user’s point of view. As the discussion unfolds, continually tie everything back to the user’s experience and needs.
* Anticipate questions that others might have, and try to answer them in your proposal comments. You don’t have to explain everything, but try to communicate a bit of your rationale every time you propose something. This is particularly important when proposing changes or challenging the status quo, because it reduces the feedback loop and time spent on unnecessary discussions. It also builds the UX Department’s credibility, because we deal with a lot of seemingly subjective issues.
* Keep the SSOT updated with what’s already agreed upon so that everyone can know where to look. This includes images or links to your design work.
* If you’re working with design files, follow the instructions in the [GitLab Design project contribution guidelines][gitlab-design-project-contribution-guidelines] and regularly commit them.
* If you are proposing a solution that will introduce a new UX paradigm, or change an existing one, please consider the following:
    1. Will this pattern be inconsistent with other areas of the application?
    1. Will other areas of the application need to be updated to match?
    1. Does this new pattern significantly improve the user experience?
    1. If you decide that it is worth changing/updating the pattern, there are additional steps that must be followed. See the [Maintain](/handbook/engineering/ux/ux-designer#maintain) section below for those steps.

#### Deliver

* Once your work is complete, make sure that the issue description, the SSOT, is updated with a section called "Solution". This is where you should direct people when they have questions about what should be done and how.
* As applicable, commit all design assets and files according to the instructions in the [GitLab Design project contribution guidelines][gitlab-design-project-contribution-guidelines].
* Whenever possible, you should provide spec-previews using the Sketch Measure plugin. Details on how to use this are located in the [design repository on the contribution page](https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md#superpowers-).
* Once UX work is completed and all feedback is addressed, remove the [UX][ux-label] label, add the [UX ready][ux-ready-label] label, and:
    * If the issue is not scheduled, mention the [responsible product manager](/handbook/product/#who-to-talk-to-for-what) to [schedule it](/handbook/engineering/workflow/#scheduling-issues)

#### Follow through

* Not all issues are scheduled immediately which means changes are likely needed when the issue is prioritized. The Product Designer responsible for a particular stage group should be aware of open issues within their product area and work to prioritize them accordingly with their respective product managers, even if they are not the original designer who worked on the issue.
    * To stay up to date with issues in your product area, subscribe to the label that matches your stage group.
    * Review issues within your stage group label regularly.
    * Actively contribute to planning meetings to ensure all open issues are being discussed and prioritized.
* When working on an issue, keep the SSOT in the description updated until the issue is closed. This applies to both text and mockups. Previous content (by a PM, for example) should be removed or archived into a separate section in the description. If the developer working on the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
* For obvious changes, make the SSOT description update directly. [You don't need to wait for consensus](/handbook/values/). Use your judgement.
* When the issue is actively being worked on, make sure you are assigned and subscribed to the issue. Continue to follow both the issue and related merge request(s), addressing any additional UX issues that come up.

#### UX Reviews

* UX is part of the MR review process.
* UX Review requests should be prioritized, tackle them as quickly as you are able.
* Test it locally, do not rely on screenshots.
* Be thorough, there should be as little back and forth as possible.
* If you are asked to review an MR for an issue you were not assigned to, remind the author who the assigned designer is and assign to original designer for review.
* When reviewing an MR, please use the following order of importance:
    * Functionality first - does it work?
    * Edge cases - are there any unexpected edge cases?
    * Visual consistency/accuracy.
* Remember to stick to the issue, create issues for further updates to avoid scope creep.

#### Maintain

If the UX work introduces or changes any of the UX standards or building blocks:

* [Create an issue](https://gitlab.com/gitlab-org/gitlab-design/issues/new?issuable_template=UX%20Pattern) in the [GitLab Design Project](https://gitlab.com/gitlab-org/gitlab-design) and follow the pattern checklist to record the new standard and update the [GitLab Pattern Library Sketch file](https://gitlab.com/gitlab-org/gitlab-design/blob/master/gitlab-pattern-library.sketch).
* Once the pattern has been added to the [GitLab Design Project](https://gitlab.com/gitlab-org/gitlab-design), create an issue and MR to add the pattern and documentation to the [GitLab Design System](https://gitlab.com/gitlab-org/design.gitlab.com).
* As applicable, create issue(s) to update areas of the application that are affected by this pattern.
* Add an agenda item to the next UX weekly call to inform everyone of those changes.
* If the change/addition is a significant one, consider adding it to the Company Call to inform the company of the changes.

#### Approach

As design can be subjective, discussion can heat up. Always try to be [direct](/handbook/values/#directness) but [kind](/handbook/values/#kindness). Try to give your best reasoning for your choices, and evaluate everyone's opinions. Come up with a solution instead of discussing endlessly. If you think additional perspective is needed, mention a fellow Product Designer in the issue.

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-contribution-guidelines]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/CONTRIBUTING.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
