---
layout: markdown_page
title: "GitLab Proof of Concept"
---

# Proof of Concept (POC) Guidelines
{:.no_toc}

GitLab wants prospects to enjoy a successful Proof of Concept with GitLab Enterprise Edition. A POC is a collaboration between GitLab and the prospective customer for evaluating GitLab Enterprise Edition. As a best practice, GitLab product evaluations should remain separate from GitLab high availability architecture and implementation evaluations.  

POC's are commonly entered into once an NDA is signed, budget for the project is in place, any competition has been identified and success criteria have been documented.

The target duration for a POC is 2 weeks, extended to a total duration of not more than 8 weeks for highly complex evaluations. A GitLab Customer Success [collaborative project](https://gitlab.com/gitlab-com/account-management/customer-collaboration-project-template) (only accessible to GitLab team members) should be utilized as the default method to manage a POC, while a [document](#POCdoc) is available when using a GitLab.com project is not an option. When utilizing a collaborative project, follow the instructions in README.md closely in order to properly copy the entire project. If a prospect has an internal process to follow, the simplified [Lite POC Document](#LITEdoc) may be used.

GitLab Solutions Architects should set a limit of 3 concurrent POC's as a best practice in order to provide excellent customer service and to enable a target of achieving 24 hour or less response times to inquiries during any POC. If more than 3 concurrent POC's are required, the SA should assess their ability to support the additional requirements and/or discuss viability with their manager as well as other SA's in their region.

## On this page
{:.no_toc}

- TOC
{:toc}

## Tracking a POC in Salesforce

### Salesforce Object

In order to track a POC correctly in Salesforce, the Strategic Account Leader should position the opportunity as Stage 3. The Solutions Architect will manually create a POC object within SFDC when the prospect or customer has indicated interest in moving forward with a guided or lite POC. 

To track a POC, click the *Proof of Concepts* tab from the top menu bar in Salesforce. Create a new POC using the *New* button. Alternately, the Solutions Architect may select the relevant Opportunity, scroll down to the related list labeled *Proof of Concepts* and click on the "New Proof of Concept" button. This will automatically associate the POC with that Opportunity while all other fields need to be manually completed. 

Complete the following fields at minimum:  
  
  * **POC Owner** - this is the Strategic Account Leader
  * **Technical Account Manager** - this is the TAM who will be part of the POC
  * **Solutions Architect** - this is the SA who owns execution of the POC
  * **Proof of Concept Name** - this commonly is named identical to the opportunity name
  * **Account** - this is the company name
  * **Opportunity** - this is the associated opportunity name (this opportunity should be in stage 3)
  * **POC Start Date** - the date the POC is expected to begin
  * **POC Close Date** - the date the POC is expected to End
  * **POC Type** - this is style POC being executed

POC Types include:

  * **Paid** - prospect has paid GitLab for a high attention (dedicated Customer Success time) or extended time evaluation of the software (> 3 months)
  * **Guided** - the most common enterprise-level POC, which follows the flow detailed on this page
  * **Lite** - for smaller opportunities requiring minimal touch points throughout the POC
  * **On-site** - intense, condensed POC executed at the customer site within a tight evaluation window (< 2 weeks)

Once the POC begins, the Solutions Architech should change the **Status** field from *New* to *In Progress*. When the POC is complete, the Solutions Architect should change the **Status** to *Closed* and the **Result** should be identified as *Successful* or *Unsuccessful*. Freeform notes should be added to support the reason for the successful or unsuccessful result.

## Enterprise POC Guide

### POC Key Players

#### Customer  

*  Executive contact - Someone with business level, budgetary buy in
*  At least one pilot team to execute the POC
*  Technical POC lead

#### GitLab Role / Responsibilities

*  Strategic Account Leader (SAL) - Relationship manager, owns temporary license, POC kickoff, SFDC admin, scheduling
*  Solutions Architect (SA) - Primary technical contact, POC lead
*  Technical Account Manager (TAM) - Project management; Ensures all tasks in playbook are done on time; included in communication and calls for visibility
*  Professional Services - only if needed in a pre-sales capacity
*  Support - only if needed for technical errors

*Note: in the case of a "lite" POC, the Solutions Architect is expected to be the sole GitLab contact. "Lite" is determined case-by-case by the size of the prospect as well as their ability to engage with GitLab.*

### POC Meetings and Tasks

#### POC Kickoff Checklist

* Solutions Architect: Ensure the customer architecture is prepared to support an on-premises POC
* Solutions Architect: Ensure customer network has access to GitLab.com
* Solutions Architect: Customer Success project is created in GitLab as per the [handbook](/handbook/customer-success/tam/#to-start-a-new-customer-engagement)
* Solutions Architect: POC document is created if this is required by the customer, otherwise default to the Customer Success project
* Solutions Architect: Ensure POC goals and business outcomes are clearly identified prior to kickoff
* Solutions Architect: Notify GitLab Support of POC dates, customer, and other relevant information
* Strategic Account Leader: Opportunity updated in Salesforce, set to Stage 3-Technical Evaluation, with POC Information entered per the [handbook](/handbook/business-ops/#opportunity-stages)
* Strategic Account Leader: Signed NDA by the legal team if required
* Strategic Account Leader: Schedule Internal kick off meeting (detailed below)
* Strategic Account Leader: Schedule kickoff meeting with customer
* Technical Account Manager: Review collaborative project content prior to internal kickoff meeting

### Meeting Cadence

Calls may be recorded with customer consent, and recordings may be stored in the Recorded Meetings folder in the project repository for mutual benefit.  

#### Internal Kickoff Meeting, led by the Solutions Architect

##### GitLab Attendees

*  Strategic Account Leader, Solutions Architect, Technical Account Manager

##### Agenda

*  Collaboratively review customer success project README to ensure everything is correct
*  POC document review (only if a document is required for the POC)
*  Discussion of strategy, whether GitLab Support or Professional Services need to be notified
*  Strategic Account Leader to schedule external kickoff with customer

#### External Kickoff Meeting (Remote), led by the Solutions Architect

##### Attendees

*  GitLab: Strategic Account Leader, Solutions Architect, Technical Account Manager
*  Customer: Executive contact, Technical POC lead

##### Agenda

*  Screen share with the customer to discuss the below agenda items
*  Collaboratively review customer success project README to ensure everything is correct
*  Agree on due dates for POC completion, add to project POC milestone
*  Review project POC issues - add new issues if necessary with agreed due dates
*  Review POC document with customer (only when required)
*  Establish cadence with customer, Strategic Account Leader to schedule during this call
*  Create issues for each cadence call with customer under the POC milestone for call notes
*  Provision licenses and establish if customer needs help getting GitLab set up and configured

#### Weekly Retrospective call, led by the Solutions Architect

##### Attendees

*  GitLab: Solutions Architect, Strategic Account Leader, Technical Account Manager (optional)
*  Customer: Technical POC Lead

##### Agenda

* What went well?
* What problems do we need to overcome?
* What do we need to change?
* Review success criteria - are we on track?

#### POC Wrap Up Meeting - Led by the Strategic Account Leader

##### Attendees

*  GitLab: Strategic Account Leader, Solutions Architect, Technical Account Manager
*  Customer: Executive contact, Technical POC Lead

##### Agenda

* Did we meet the success criteria?
* Did we meet the goals for the customer? Is this a technical win?
* Next steps
* Send out POC survey

### <a name="POCdoc"></a>POC Template Document

As an alternative (or in addition) to using a collaborative GitLab project, a document is available which helps outline the details of a POC. In the [document](https://docs.google.com/document/d/1anbNik_H_XDHJYyZPkfr4_6wvIXgqbwvWynN9v9tj2E/edit#) (only accessible to GitLab team members), provides the framework for a successful POC by addressing current state issues, persistent challenges, business problems, desired state and outcomes.  

This document suggests and verifies specific success criteria for any POC, as well as outlining a mutual commitment between GitLab and the identified prospect parties. It also specifies the limited timeframe in which the POC will occur.  

### Using the POC Template

The template provides a standardized approach to the POC process, but the document requires customization for each and every POC to be executed.  

To use the template, begin by making a copy of the master document for each POC.  

Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team-member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location, and the client pilot team(s).  

After all the highlighted sections have been completely filled in, collaboratively complete the **Date** and **Owner** column fields within the **Project Plan** and **Roles and Responsibilities** sections.  

Finally, ensure both GitLab and the prospect have a copy of the document. Schedule all meetings associated to the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.

## Commercial Sales POC Guide

### Kick Off Meeting
* Duration: 30 Minutes
* Attendees: GitLab Account Executive, GitLab Solutions Architect, Prospective Buyer(s)
* Agenda:
  * Define success criteria (as a best practice, have no more than 5 unique success criteria deliverables)
  * Confirm start and end date
  * Determine communication method
    * Customer to decide if they are interested in communicating on a collaborative GitLab project or email only
    * Frequency options: 30 minute weekly call or email touchbase weekly with calls scheduled as needed
  * Outline the SA role
    * Acts as the primary point of contact throughout POC process
    * Tracks status updates regarding sucess criteria
    * Records any existing or new feature requests of interest to customer
    * Leads troubleshooting and escalation of blocks


### Commercial Sales - Customer Collaboration Project Process
* [Track POC in Salesforce according to this process](/handbook/sales/POC/#tracking-a-proof-of-concept-in-salesforce)
* Create [POC project](https://gitlab.com/gitlab-com/account-management/commercial/pre-sales) in Commercial/Pre-sales group by selecting Create from Template > Group > Project-Template-Commercial-Sales
* Edit README.md with information specific to POC under Proof of Concept section
* Upon completion of POC, update Salesforce record with POC result as successful or unsuccessful and provide supportive reasons in the associated freeform fields
  * For a successful POC:
    * SA to move project from Pre-sales folder to [Commercial](https://gitlab.com/gitlab-com/account-management/commercial)
    *  SA and AE to schedule meeting with new customer to walk through README.md. The intended outcome is to confirm the account information specific to their GitLab instance is accurate in the customer collaboration project.
    * TAM to determine TAM eligibility and engagement based on Account Tier and IACV.


## <a name="LITEdoc"></a>LITE POC Template Document

When a prospect has an internal POC process to follow, or when time is of the essence, the SA may utilize the [LITE POC document](https://docs.google.com/document/d/1PO3jXG3wiKsCbx5vb8dm4SmOe_PiTB47SadROIO8nCc/edit#) (only accessible to GitLab team-members), which validates POC dates, success criteria and assumptions of both GitLab and the prospect.

### Using the LITE POC Template

The template provides a simplified approach to the POC process, but the document requires customization for each POC to be executed.  

To use the template, begin by making a copy of the master document for each POC.  

Edit each area highlighted in yellow within the document to include pertinent information for any particular prospect. This information includes basic data like the prospect name and GitLab team-member details, as well as data to be collaboratively identified with the prospect, such as success criteria, trial location and the client pilot team(s). Delete any red-colored instructional text.  

Finally, ensure both GitLab and the prospect have a copy of the document. Schedule weekly meetings for the duration of the POC via calendar invites prior to distributing the GitLab Enterprise Edition license for the POC.
